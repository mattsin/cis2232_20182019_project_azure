/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ojt.hccis.business.test;

import info.hccis.ojt.model.jpa.Business;
import java.io.IOException;
import java.lang.reflect.Field;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClientBuilder;
import static org.hamcrest.CoreMatchers.equalTo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rodrigo
 */
public class BusinessTest {
    
    public BusinessTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    //Check if the URI (webservice) is working  and accessible
    @Test
    public void giveWebServiceURIExisting_whenUserInfoIsRetrieved_then200IsReceived()
    throws ClientProtocolException, IOException {
  
        // Given

        HttpUriRequest request = new HttpGet( "http://localhost:8080/ojt/rest/BusinessService/business/showall");

        // When
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute( request );

        // Then
        assertThat(
          httpResponse.getStatusLine().getStatusCode(),
          equalTo(HttpStatus.SC_OK));
    }
    
    //Check if the webservice is responding to requests with a Json file.
    @Test
    public void givenRequestWithNoAcceptHeader_whenRequestIsExecuted_thenDefaultResponseContentTypeIsJson()
    throws ClientProtocolException, IOException {
  
        // Given
        String jsonMimeType = "application/json";
        HttpUriRequest request = new HttpGet( "http://localhost:8080/ojt/rest/BusinessService/business/showall" );

        // When
        HttpResponse response = HttpClientBuilder.create().build().execute( request );

        // Then
        String mimeType = ContentType.getOrDefault(response.getEntity()).getMimeType();
        assertEquals( jsonMimeType, mimeType );
    }
    
    //Check if the setter "name" of Business entitu is working
    @Test
    public void testSetterNameInBusiness_setsProperly() throws NoSuchFieldException, IllegalAccessException {
        //given
        final Business bTest = new Business();

        //when
        bTest.setName("Rodrigo");

        //then
        final Field field = bTest.getClass().getDeclaredField("name");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(bTest), "Rodrigo");
    }
    
   //Check if the setter "username" of Business entitu is working
    @Test
    public void testSetterUserNameInBusiness_setsProperly() throws NoSuchFieldException, IllegalAccessException {
        //given
        final Business bTest = new Business();

        //when
        bTest.setUsername("rlatorraca@gmail.com");

        //then
        final Field field = bTest.getClass().getDeclaredField("username");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(bTest), "rlatorraca@gmail.com");
    }
    
    //Check if the getter "name" of Business entitu is working
    @Test
     public void testGetterNameInBusiness_getsValue() throws NoSuchFieldException, IllegalAccessException {
        //given
        final Business bTest = new Business();
        final Field field = bTest.getClass().getDeclaredField("name");
        field.setAccessible(true);
        field.set(bTest, "getValues");

        //when
        final String result = bTest.getName();

        //then
        assertEquals("field wasn't retrieved properly", result, "getValues");
    }



}
