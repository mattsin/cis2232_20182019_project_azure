/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.student.test;

import info.hccis.ojt.model.jpa.Student;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Matt B
 */
public class StudentTest {
    
    public StudentTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    
Student newStudent = new Student(1);


// Student ID Test
@Test
public void test_studentID(){
    
    int test = newStudent.getId();
    assertEquals(1, test, .002);
}


// Student First Name Test
@Test
public void test_studentFisrtName(){
    
    newStudent.setFirstName("Matt");
    String test = newStudent.getFirstName();
    assertEquals("Matt", test);
}

// Student Last Name Test
@Test
public void test_studentLastName(){
    
    newStudent.setLastName("Bryan");
    String test = newStudent.getLastName();
    assertEquals("Bryan", test);
}

// Student Username Test
@Test
public void test_studentUsername(){
    
    newStudent.setUsername("mbyran@hollandcollege.com");
    String test = newStudent.getUsername();
    assertEquals("mbyran@hollandcollege.com", test);
}

// Student Phone Test
@Test
public void test_studentPhone(){
    
    newStudent.setPhone("902-439-9813");
    String test = newStudent.getPhone();
    assertEquals("902-439-9813", test);
}


}
