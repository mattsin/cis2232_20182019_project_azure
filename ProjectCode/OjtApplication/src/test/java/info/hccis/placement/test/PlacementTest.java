package info.hccis.placement.test;

import info.hccis.ojt.model.jpa.Placement;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * This is a placement test file; it is used to test the Placement class attributes
 *
 * @author Arvind Chauhan
 * @since 20181202
 */
public class PlacementTest {
    
    private Placement placement = new Placement();
    
    public PlacementTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class Placement.
     * @author: Arvind Chauhan
     * @since 20181202
     */
    @Test
    public void testGetPlacementId() {
        System.out.println("getPlacementId");
        placement = new Placement();
        Integer expResult = 1;
        placement.setId(expResult);
        Integer result = placement.getId();
        assertEquals("Fields didn't match",expResult, result);
    }

    /**
     * Test of setId method, of class Placement.
     * @author: Arvind Chauhan
     * @since 20181202
     */
    @Test
    public void testSetPlacementId() {
        System.out.println("setPlacementId");
        Integer expResult = 3;
        placement = new Placement();
        placement.setId(expResult);
        Integer result = placement.getId();
        assertEquals("Fields didn't match",expResult, result);
    }


    /**
     * Test of getBusinessId method, of class Placement.
     * @author: Arvind Chauhan
     * @since 20181202
     */
    @Test
    public void testGetBusinessId() {
        System.out.println("getBusinessId");
        placement = new Placement();
        Integer expResult = 1;
        placement.setBusinessId(expResult);
        Integer result = placement.getBusinessId();
        assertEquals("Fields didn't match",expResult, result);
    }

    /**
     * Test of getPlacementDate method, of class Placement.
     * @author: Arvind Chauhan
     * @since 20181202
     */
    @Test
    public void testGetPlacementDate() {
        System.out.println("getPlacementDate");
        placement = new Placement();
        String expResult = "20180909";
        placement.setPlacementDate(expResult);
        String result = placement.getPlacementDate();
        assertEquals("Fields didn't match",expResult, result);
    }

    /**
     * Test of getNotes method, of class Placement.
     * @author: Arvind Chauhan
     * @since 20181202
     */
    @Test
    public void testGetNotes() {
        System.out.println("getNotes");
        placement = new Placement(1);
        String expResult = "Test Notes";
        placement.setNotes(expResult);
        String result = placement.getNotes();
        assertEquals("Fields didn't match",expResult, result);
    }
    
}
