/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.ojt.web;

import info.hccis.ojt.bo.UserBO;
import info.hccis.ojt.dao.BusinessDAO;
import info.hccis.ojt.data.springdatajpa.BusinessRepository;

import info.hccis.ojt.model.jpa.Business;
import info.hccis.ojt.model.jpa.User;
import info.hccis.ojt.util.Utility;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author: Rodrigo Pires
 * @since: 20181030
 * @Usage: grab info from "business.csv" file and pass to insert method in
 * BusinessDAO.java
 */
@Controller
public class BusinessController {

    private final BusinessRepository br;

    /**
     * This constructor will inject the UserRepository object into this
     * controller. It will allow this class' methods to use methods from this
     * Spring JPA repository object.
     *
     * @since 20180524
     * @author BJM
     * @param br
     */
    @Autowired
    public BusinessController(BusinessRepository br) {
        this.br = br;
    }

    private static final String FILE_NAME = "/cis2232/businessList.csv";

    /**
     * @Author RodrigoPires
     * @since 20181010
     * This method import companies names from the /cis2232/businessList.csv e print in the screen
     * Used the view /business/import
     * 
     * @param model
     * @param theBusinessFromTheForm
     * @param result
     * @return
     * @throws SQLException
     * @throws IOException 
     */
    
    @RequestMapping("/business/import")
    public String importBusiness(Model model, @ModelAttribute("businessArray") Business theBusinessFromTheForm, BindingResult result) throws SQLException, IOException {

        try {
            //Put the file information inside the ArrayList business
            ArrayList<String> business = (ArrayList<String>) Files.readAllLines(Paths.get(FILE_NAME));

            //created the BusinessDAO object
            BusinessDAO tmpBusinessDAO = new BusinessDAO();

            //call the method insert() and send the business ArrayList
            tmpBusinessDAO.insert(business);

        } catch (IOException ex) {

            System.out.println(ex.getMessage());
        }
        //call the method selectAll() 
        model.addAttribute("businessArray", BusinessDAO.selectAll());

        //This will send the user to the other/impExpConfirmationBusiness.html page.
        return "other/business/impExpConfirmationBusiness";
    }

    @RequestMapping("/business/list")
    public String showBusinessList(Model model) {

        //Get the campers from the database
        model.addAttribute("Business", br.findAll());

        //This will send the user to the list page
        return "other/business/businessList";
    }

    @RequestMapping("/business/add")
    public String businessAdd(Model model) {

        //put a camper object in the model to be used to associate with the input tags of 
        //the form.
        Business newBusiness = new Business();
        model.addAttribute("Business", newBusiness);

        //This will send the user to the welcome.html page.
        return "other/business/businessAdd";
    }

    @RequestMapping("/business/update")
    public String businessUpdate(Model model, HttpServletRequest request) {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        //- It will go to the database and load the camper details into a camper
        //  object and put that object in the model.  

        //*******************************************************************
        //Use Spring data JPA instead
        Business editBusiness = br.findOne(Integer.parseInt(idToFind));

        model.addAttribute("Business", editBusiness);
        return "other/business/businessAdd";

    }

    @RequestMapping("/business/delete")
    public String businessDelete(Model model, HttpServletRequest request) {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");

        br.delete(Integer.parseInt(idToFind));

        //Reload the Business list so it can be shown on the next view.
        model.addAttribute("Business", br.findAll());
        return "other/business/businessList";

    }

    @RequestMapping("/business/addSubmit")
    public String businessAddSubmit(Model model, @Valid @ModelAttribute("Business") Business theBusinessFromTheForm, BindingResult result, HttpSession session) {

        // Prevent URL manipulation
        User loggedInUser = (User) session.getAttribute("loggedInUser");

        if (!(loggedInUser.getUsername().equals(theBusinessFromTheForm.getUsername()) || loggedInUser.getUserTypeCode() == 1)) {

            User user = new User();

            if (Utility.DEBUGGING) {
                user.setUsername("admin@hccis.info");
                user.setPassword("123");
            }

            //Add a user object to be bound to controls on the view.
            model.addAttribute("user", user);
            model.addAttribute("errorMessage", "Insufficient Privileges");

            return "/other/welcome";
        }

        if (Utility.TESTING) {
            System.out.println("RLSP-Checking validation." + result.getErrorCount());
        }

        //You can do your own validation if you need to.x`
        String error = UserBO.validateEmail(theBusinessFromTheForm);
        String errorPositions = UserBO.validatePositions(theBusinessFromTheForm);

        if (result.hasErrors() || error.length() > 0 || errorPositions.length() > 0) {
            System.out.println("Error in validation.");
            if (error.length() > 0) {
                model.addAttribute("message", error);
            }

            if (errorPositions.length() > 0) {
                model.addAttribute("messagePositions", errorPositions);

            }

            return "/other/business/businessAdd";
        }

        //Call the dao method to put this guy in the database.
        try {

            br.save(theBusinessFromTheForm);

        } catch (Exception e) {

            System.out.println("Could not save to the database");
        }

        long size = br.count();
        model.addAttribute("message", "Business updated.  There are now " + size + " companies");

        model.addAttribute("Business", br.findAll());

        //This will send the user to the Business List page.
        return "other/business/businessList";
    }

    //List Unready Business List that do not have confirmation letters or liability indemnifications sent
    @RequestMapping("/business/unreadBusiness")
    public String showUnreadyBusiness(Model model) {

        //Get the unread companies (that do not have confirmation letters or liability indemnifications sent)
        //from the database
        model.addAttribute("Business", br.findByUnreadBusiness());

        //This will send the user to the Unready Business List page
        return "other/business/unreadyBusinessList";
    }
}
