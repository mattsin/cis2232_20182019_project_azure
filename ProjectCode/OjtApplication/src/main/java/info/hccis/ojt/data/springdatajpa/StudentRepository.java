/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.ojt.data.springdatajpa;

import info.hccis.ojt.model.jpa.Student;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 *
 */
@Repository
public interface StudentRepository extends CrudRepository<Student, Integer> {

    List<Student> findByUsername(String username);
    List<Student> findByValidResume();
    
}
