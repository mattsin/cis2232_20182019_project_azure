package info.hccis.ojt.web;

import info.hccis.ojt.dao.CodeValueDAO;
import info.hccis.ojt.data.springdatajpa.UserRepository;
import info.hccis.ojt.model.DatabaseConnection;
import info.hccis.ojt.model.jpa.CodeValue;
import info.hccis.ojt.model.jpa.User;
import info.hccis.ojt.util.Utility;
import info.hccis.ojt.bo.UserBO;
import java.util.ArrayList;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OtherController {

    private final UserRepository ur;

    /**
     * This constructor will inject the UserRepository object into this
     * controller. It will allow this class' methods to use methods from this
     * Spring JPA repository object.
     *
     * @since 20180524
     * @author BJM
     * @param ur
     */
    @Autowired
    public OtherController(UserRepository ur) {
        this.ur = ur;
    }

    /**
     * This mapping will handle the root of the application. It will send the
     * user to the welcome page and allow them a starting place to use the
     * application.
     *
     * @since 20180524
     * @author BJM
     *
     * @param model
     * @param session
     * @param request
     * @return
     */
    @RequestMapping("/")
    public String showHome(Model model, HttpSession session, HttpServletRequest request) {

        //**********************************************************************
        // Testing the Utility.getMessage(key) method. 
        //**********************************************************************
        System.out.println("label.help=" + Utility.getMessage("label.help"));

        //*********************************************************************
        // Adding some code here to demonstrate some of the locale functionality
        // available.
        //********************************************************************
        Locale currentLocale = request.getLocale();
        System.out.println("locale test" + currentLocale.getDisplayLanguage());
        System.out.println("locale test" + currentLocale.getDisplayCountry());
        System.out.println("locale test" + currentLocale.getLanguage());
        System.out.println("locale test" + currentLocale.getCountry());

        Locale frenchLocale = new Locale("en", "CA");
        System.out.println("locale test2" + frenchLocale.getDisplayLanguage());
        System.out.println("locale test2" + frenchLocale.getDisplayCountry());
        System.out.println("locale test2" + frenchLocale.getLanguage());
        System.out.println("locale test2" + frenchLocale.getCountry());
        Locale.setDefault(frenchLocale);

        //**********************************************************************
        // We are heading to the welcome/login page.  Set a new User object in 
        // the model.  It is this object which will be bound to the username/pw
        // attributes on the view.
        //**********************************************************************
        User user = new User();

        if (Utility.DEBUGGING) {
            user.setUsername("admin@hccis.info");
            user.setPassword("123");
        }

        //Add a user object to be bound to controls on the view.
        model.addAttribute("user", user);

        //Clear any possible existing loggedInUser.  
        session.removeAttribute("loggedInUser");
        
        
//        session.setAttribute("loggedInUser", new User());

        //This will send the user to the welcome.html page.
        return "other/welcome";
    }

    @RequestMapping("/logout")
    public String logout(Model model, HttpSession session) {

        //**********************************************************************
        // Similar to going to the welcome page, create a new User object and
        // clear the loggedInUser from the session.
        //**********************************************************************
        User temp = (User) session.getAttribute("loggedInUser");
        model.addAttribute("user", new User());
        session.removeAttribute("loggedInUser");

        //**********************************************************************
        //Give a message indicating that they have been logged out.
        //If getting here without a first name set in the user object, set it to
        //empty string so 'null' doesn't show up on the message.
        //**********************************************************************
        if(temp == null || temp.getFirstName() == null){
            temp = new User();
            temp.setFirstName("");
            
        }
        model.addAttribute("notificationMessage", Utility.getMessage("logout.success", temp.getFirstName()));

        //This will send the user to the welcome.html page.
        return "other/welcome";
    }

    /**
     * This method will use the user object associated with the welcome page.
     *
     * @since 20180524
     * @author BJM
     *
     * @param model
     * @param user
     * @param session
     * @return
     */
    @RequestMapping("/authenticate")
    public String authenticate(Model model, @ModelAttribute("user") User user, HttpSession session) {

        session.removeAttribute("loggedInUser");

        if (!UserBO.authenticate(user, ur)) {

            //*******************************************
            //failed validation
            //*******************************************
            session.setAttribute("loggedInUser", user);
            model.addAttribute("errorMessage", Utility.getMessage("login.failed"));
            return "other/welcome";
        } else {

            //*******************************************
            //passed validation 
            //Get the campers from the database
            //send to the list page (camper/list)
            //*******************************************
            user = UserBO.getUserByUsername(user, ur);
            session.setAttribute("loggedInUser", user);

            //Load the camper types from CodeValue for use when adding a camper
            ArrayList<CodeValue> campTypes = CodeValueDAO.getCodeValues(new DatabaseConnection(), "2");
            System.out.println("BJM - added " + campTypes.size() + " to the session for camp types");
            session.setAttribute("CampTypes", campTypes);

            //Give a message indicating that they have been logged out.
            model.addAttribute("notificationMessage", Utility.getMessage("welcome", user.getFirstName()));

            //******************************************************************
            //Send the user to the landing page
            //******************************************************************
            return "other/about";
        }
    }

    @RequestMapping("/about")
    public String showAbout(Model model) {
        return "other/about";
    }

    @RequestMapping("/help")
    public String showHelp(Model model) {
        return "other/help";
    }

}
