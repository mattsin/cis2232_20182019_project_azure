package info.hccis.ojt.util;

import info.hccis.ojt.dao.CodeValueDAO;
import info.hccis.ojt.model.DatabaseConnection;
import info.hccis.ojt.model.jpa.CodeType;
import info.hccis.ojt.model.jpa.CodeValue;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;

/**
 * This class will be used to hold generic code related methods.
 *
 * @author bjmaclean
 * @since 20170503
 */
public class UtilCodes {

    public static void updateSessionCodes(HttpServletRequest request) {
        DatabaseConnection databaseConnection = (DatabaseConnection) request.getSession().getAttribute("db");

        ArrayList<CodeValue> UserTypes = (ArrayList<CodeValue>) CodeValueDAO.getCodeValues(databaseConnection, "1");
        request.getSession().setAttribute("UserTypes", UserTypes);
        ArrayList<CodeValue> golfCourseTypes = (ArrayList<CodeValue>) CodeValueDAO.getCodeValues(databaseConnection, "2");
        request.getSession().setAttribute("golfCourseTypes", golfCourseTypes);
        ArrayList<CodeValue> chicagoPointTypes = (ArrayList<CodeValue>) CodeValueDAO.getCodeValues(databaseConnection, "3");
        request.getSession().setAttribute("chicagoPointTypes", chicagoPointTypes);

        int[] points = new int[chicagoPointTypes.size()];
        for (int i = 0; i < chicagoPointTypes.size(); i++) {
            points[i] = Integer.parseInt(chicagoPointTypes.get(i).getEnglishDescription());
        }
        System.out.println("chicagoPoints=" + points);
        request.getSession().setAttribute("chicagoPoints", points);
    }

}
