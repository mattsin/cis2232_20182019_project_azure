/**
 * @author: Matt B
 * @since: 20181030
 * @Usage: StudentController to pull student info from csv file.
 */
package info.hccis.ojt.web;

import info.hccis.ojt.dao.StudentDAO;
import info.hccis.ojt.data.springdatajpa.StudentRepository;
import info.hccis.ojt.model.jpa.Student;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author: Matt Bryan
 * @since: 20181030
 * @Usage: grab info from "student.csv" file, split, and pass to update method
 * in StudentDAO
 */
@Controller
public class StudentController {

    @RequestMapping("/student")
    public String importStudents(Model model) {

        String FILE_NAME = "/cis2232/studentList.csv";
        String[] student = new String[3];

        try {
            ArrayList<String> test = (ArrayList<String>) Files.readAllLines(Paths.get(FILE_NAME));

            for (String current : test) {
                student = current.split(",");
                StudentDAO.update(student[0], student[1], student[2]);
            }

        } catch (IOException ex) {
            System.out.println("Error loading students from file.");
            System.out.println(ex.getMessage());
        }

        return "other/impExpConfirmationStudent";
    }

    private final StudentRepository sr;

    /**
     * This constructor will inject the StudentRepository object into this
     * controller. It will allow this class methods to use methods from this
     * Spring JPA repository object.
     *
     * @param sr
     * @since 20181113
     * @author Matt B
     */
    @Autowired
    public StudentController(StudentRepository sr) {
        this.sr = sr;
    }

    
    // Add Student
    @RequestMapping("/ojtOperations/studentAdd")
    public String studentAdd(Model model) {

        Student newStudent = new Student();
        model.addAttribute("student", newStudent);

        //This will send the user to the studentAdd.html page.
        return "ojtOperations/studentAdd";
    }

    // Update Student
    @RequestMapping("/ojtOperations/update")
    public String studentUpdate(Model model, HttpServletRequest request) {

        String idToFind = request.getParameter("id");
        Student editSudent = sr.findOne(Integer.parseInt(idToFind));
        model.addAttribute("student", editSudent);

        // This will send the user to the studentAdd.html page
        return "/ojtOperations/studentAdd";

    }

    // Delete Student
    @RequestMapping("/ojtOperations/delete")
    public String studentDelete(Model model, HttpServletRequest request) {

        String idToFind = request.getParameter("id");

        sr.delete(Integer.parseInt(idToFind));
        model.addAttribute("students", sr.findAll());

        // This will send the user to the studentList.html page
        return "/ojtOperations/studentList";

    }

    @RequestMapping("/ojtOperations/addSubmit")
    public String ojtreflectionAddSubmit(Model model, @Valid @ModelAttribute("students") Student theStudentFromTheForm, BindingResult result) {

        if (result.hasErrors()) {
            System.out.println("Error in validation.");
            return "/ojtOperations/studentAdd";
        }
        
        try {
            sr.save(theStudentFromTheForm);
        } catch (Exception e) {
            System.out.println("Could not save to the database");
        }
        model.addAttribute("students", sr.findAll());

        //This will send the user to the studentList.html page.
        return "ojtOperations/studentList";
    }

    //List Students
    @RequestMapping("/ojtOperations/studentList")
    public String showStudents(Model model) {

        long size = sr.count();
        model.addAttribute("message", "Showing " + size + " of " + size + "");

        //Get the reflections from the database
        model.addAttribute("students", sr.findAll());

        //This will send the user to the studentList page
        return "ojtOperations/studentList";
    }

    //List studnets with resumes and cover letters
    @RequestMapping("/ojtOperations/studentsReadyList")
    public String showReadyStudents(Model model) {

        //Get the reflections from the database
        model.addAttribute("students", sr.findByValidResume());

        //This will send the user to the studentList page
        return "ojtOperations/studentList";
    }

}
