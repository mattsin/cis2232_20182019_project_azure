package info.hccis.ojt.data.springdatajpa;

import info.hccis.ojt.model.jpa.Business;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Rodrigo
 * @since 20181110
 * This 
 */
@Repository
public interface BusinessRepository extends CrudRepository<Business, Integer> {

    //Can add specific methods such as these
    List<Business> findByUnreadBusiness();
    List<Business> findByUsername(String username);
    List<Business> findByName (String name);

}