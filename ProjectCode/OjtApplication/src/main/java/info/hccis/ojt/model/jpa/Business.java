/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.ojt.model.jpa;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "business")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Business.findAll", query = "SELECT b FROM Business b")
    , @NamedQuery(name = "Business.findById", query = "SELECT b FROM Business b WHERE b.id = :id")
    , @NamedQuery(name = "Business.findByUsername", query = "SELECT b FROM Business b WHERE b.username = :username")
    , @NamedQuery(name = "Business.findByName", query = "SELECT b FROM Business b WHERE b.name = ?")
    , @NamedQuery(name = "Business.findByPhoneNumber", query = "SELECT b FROM Business b WHERE b.phoneNumber = :phoneNumber")
    , @NamedQuery(name = "Business.findByWebsite", query = "SELECT b FROM Business b WHERE b.website = :website")
    , @NamedQuery(name = "Business.findByConfirmLetterSentDate", query = "SELECT b FROM Business b WHERE b.confirmLetterSentDate = :confirmLetterSentDate")
    , @NamedQuery(name = "Business.findByUnreadBusiness", query = "SELECT b FROM Business b WHERE b.confirmLetterSentDate = '' OR b.liabilityIdemnificationSentDate ='' OR b.confirmLetterSentDate is null OR b.liabilityIdemnificationSentDate is null")
    , @NamedQuery(name = "Business.findByLiabilityIdemnificationSentDate", query = "SELECT b FROM Business b WHERE b.liabilityIdemnificationSentDate = :liabilityIdemnificationSentDate")
    , @NamedQuery(name = "Business.findByNumberOfPositions", query = "SELECT b FROM Business b WHERE b.numberOfPositions = :numberOfPositions")})
public class Business implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(min = 5, max = 100)    
    @Column(name = "username")
    private String username;
    @Size(min = 5,max = 50)
    @Column(name = "name")
    private String name;
    @Size(min = 7,max = 10)
    @Column(name = "phoneNumber")
    private String phoneNumber;
    @Size(min = 5,max = 100)
    @Column(name = "website")
    private String website;
    @Size(max = 10)
    @Column(name = "confirmLetterSentDate")
    private String confirmLetterSentDate;
    @Size(max = 10)
    @Column(name = "liabilityIdemnificationSentDate")
    private String liabilityIdemnificationSentDate;  
    
    @Column(name = "numberOfPositions")
    private Integer numberOfPositions;

    public Business() {
    }

    public Business(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getConfirmLetterSentDate() {
        return confirmLetterSentDate;
    }

    public void setConfirmLetterSentDate(String confirmLetterSentDate) {
        this.confirmLetterSentDate = confirmLetterSentDate;
    }

    public String getLiabilityIdemnificationSentDate() {
        return liabilityIdemnificationSentDate;
    }

    public void setLiabilityIdemnificationSentDate(String liabilityIdemnificationSentDate) {
        this.liabilityIdemnificationSentDate = liabilityIdemnificationSentDate;
    }

    public Integer getNumberOfPositions() {
        return numberOfPositions;
    }

    public void setNumberOfPositions(Integer numberOfPositions) {
        this.numberOfPositions = numberOfPositions;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Business)) {
            return false;
        }
        Business other = (Business) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.camper.util.Business[ id=" + id + " ]";
    }
    
}
