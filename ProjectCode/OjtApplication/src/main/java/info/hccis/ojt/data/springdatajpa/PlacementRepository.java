package info.hccis.ojt.data.springdatajpa;

import info.hccis.ojt.model.jpa.Placement;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlacementRepository extends CrudRepository<Placement, Integer> {
        
//        List<Placement> findByStudentId(int studentId);
//        List<Placement> findByBusinessId(int businessId);
//        List<Placement> findUnplacedStudent();
//        List<Placement> findUnplacedBusiness();
}
