/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.ojt.dao;

import info.hccis.ojt.model.jpa.Business;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * @author: Matt Bryan 
 * @since: 20181030
 * @Usage: Update method to add students based on info pulled from
 * "business.csv" (See BusinessController).
 */
public class BusinessDAO {
    
    private static Connection conn = null;
    private static Statement stmt;
    private static PreparedStatement theStatement;
    
    
    /**
     * Default constructor will setup the connection and statement objects to be
     * used by this CamperDAO instance.
     *
     * @since 20180928
     * @author BJM
     */
    public BusinessDAO() {
        try {
            conn = ConnectionUtils.getDBConnection();         
            stmt = conn.createStatement();
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();

    }
    }
    
    
    public static ArrayList<Business> selectAll() throws SQLException {

        ArrayList<Business> businessArray = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;        
        
        try {
            sql = "SELECT * FROM business order by id";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {            
                int id =  rs.getInt("id");
                String username = rs.getString("username");
                String name = rs.getString("name");
                String phoneNumber = rs.getString("phoneNumber");
                String website = rs.getString("website");
                String confirmLetterSentDate = rs.getString("confirmLetterSentDate");
                String liabilityIdemnificationSentDate = rs.getString("liabilityIdemnificationSentDate");
                int numberOfPositions = rs.getInt("numberOfPositions");                
 
                Business bus = new Business();
                bus.setId(id);
                bus.setUsername(username);
                bus.setName(name);
                bus.setPhoneNumber(phoneNumber);
                bus.setWebsite(website);
                bus.setConfirmLetterSentDate(confirmLetterSentDate);
                bus.setLiabilityIdemnificationSentDate(liabilityIdemnificationSentDate);
                bus.setNumberOfPositions(numberOfPositions);
                businessArray.add(bus);
                
                
            }
            

        
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(theStatement, conn);
        }
       
        return businessArray;
    }
    
    
    /**
     * This method will insert the input Business files. It will notify the console if
     * an exception is encountered (IE student already exists).
     *
     * @param company
     * @throws java.sql.SQLException
     * @since 20180928
     * @author BJM
     */
    public void insert(ArrayList<String> company) throws SQLException {
        
        try{
           
            theStatement = null;        

            String sql = "INSERT INTO business (username,name,phoneNumber) VALUES (?,?,?)";
            
            theStatement = conn.prepareStatement(sql);                   
            for(String line : company){
                String[]field = line.split(",");                                             
                theStatement.setString(1, field[0]);
                theStatement.setString(2, field[2]);
                theStatement.setString(3, field[1]);
                theStatement.addBatch();                                                               
            } 
                            
               theStatement.executeUpdate();

            theStatement.executeBatch();

        }catch (SQLException sqle) {
            System.out.println("Could not add Companies");
            System.out.println(sqle.getMessage());
        }

    }
}
