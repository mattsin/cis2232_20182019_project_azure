package info.hccis.ojt.bo;

import info.hccis.ojt.data.springdatajpa.UserRepository;
import info.hccis.ojt.model.jpa.Business;
import info.hccis.ojt.model.jpa.User;
import info.hccis.ojt.util.Utility;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class will contain functionality related to working with user objects.
 *
 * @author bjmaclean
 * @since Nov 3, 2017
 */
public class UserBO {

    public static boolean authenticate(User user, UserRepository ur) {
        //Check that the user exists in the user table
        ArrayList<User> usersFromDB = (ArrayList<User>) ur.findByUsername(user.getUsername());
        boolean authenticated = false;
        boolean authenticatedUsername = false;

        if (usersFromDB.size() == 1) {
            authenticatedUsername = true;
        }

        //if they exist verify that the password hash matches the hashed pw in the db
        String hashedEnteredPassword = Utility.getHashPassword(user.getPassword());

        if (authenticatedUsername
                && usersFromDB.get(0).getPassword().equals(hashedEnteredPassword)) {
            authenticated = true;
        }

        return authenticated;

    }

    public static User getUserByUsername(User user, UserRepository ur) {

        ArrayList<User> usersFromDB = (ArrayList<User>) ur.findByUsername(user.getUsername());
        if (usersFromDB.size() > 0) {
            return usersFromDB.get(0);
        } else {
            return null;
        }

    }

    public static boolean verifyLoggedIn(User user) {

        try {
            if (user.getUserTypeCode() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
 public static String validateEmail(Business business)
    {
        String errorEmail = "";
        String email = business.getUsername();
        System.out.println(email.length());
        try{
            if (email.length() > 0) {
            String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(email);
                if (!matcher.matches()) {
                    errorEmail = "Enter a correct email pattern";
                }
            }else if (email.length() == 0){                
                    errorEmail = "Enter a username";
            }
        } catch(Exception e) {
             errorEmail = "Enter a correct email pattern";
        }

        return errorEmail;
    }
    
    
        public static String validatePositions(Business business)
    {
        String errorPosition = "";
        int positions;
        
        
         
        try{
            if (business.getNumberOfPositions()!=null){
            positions = business.getNumberOfPositions();
                if ( positions <= 0) {               
                    errorPosition = "Enter a at least 1 spot";
                }  
            } else{
                errorPosition = "Enter a at least 1 spot";
            }         

        } catch(Exception e) {
             errorPosition = "Enter a correct email pattern";
        }

        return errorPosition;
    }
    
}
