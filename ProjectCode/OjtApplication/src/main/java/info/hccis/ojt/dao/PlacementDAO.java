
package info.hccis.ojt.dao;

import info.hccis.ojt.model.jpa.Placement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
/**
 * @author: Arvind Chauhan 
 * @since: 20181102
 * @Usage: Update method to retrieve placement info based 
 * info residing in database (See PlacementController).
 */
public class PlacementDAO {
 
    private static Connection conn = null;
    private static Statement stmt;
    private static PreparedStatement theStatement;
    
//    private static final String URL = "jdbc:mysql://localhost:3306/cis2232_ojt";
//    private static final String USERNAME = "cis2232_admin";
//    private static final String PASSWORD = "Test1234";
    
    /**
     * Default constructor will setup the connection and statement objects to be
     * used by this CamperDAO instance.
     *
     * @since 20180928
     * @author BJM
     */
    
        public PlacementDAO() {
        try {
            conn = ConnectionUtils.getDBConnection();
            //conn = DriverManager.getConnection(URL,USERNAME,PASSWORD);            
            stmt = conn.createStatement();
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();

    }
    }
        
    public static ArrayList<Placement> selectAll() throws SQLException {

        ArrayList<Placement> placementArray = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;        
        
        try {
            sql = "SELECT * FROM placement order by id";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {            
                int id =  rs.getInt("id");
                int studentId = rs.getInt("studentId");
                int businessId = rs.getInt("businessId");
                String placementDate = rs.getString("placementDate");
                String notes = rs.getString("notes");               
 
                Placement placement = new Placement();
                placement.setId(id);
                placement.setStudentId(studentId);
                placement.setBusinessId(businessId);
                placement.setPlacementDate(placementDate);
                placement.setNotes(notes);
                placementArray.add(placement);
            }
            
            
        
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(theStatement, conn);
        }
        System.out.println("TESTE SelectAll 3");
        return placementArray;
    }

    public void insert(ArrayList<String> placement) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
