
package info.hccis.ojt.web;

import info.hccis.ojt.data.springdatajpa.BusinessRepository;
import info.hccis.ojt.data.springdatajpa.PlacementRepository;
import info.hccis.ojt.data.springdatajpa.StudentRepository;
import info.hccis.ojt.model.jpa.Placement;
import info.hccis.ojt.model.jpa.Student;
import info.hccis.ojt.model.jpa.Business;
import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.DriverManager;
import java.util.*;
import java.lang.*;
import java.text.*;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author: Arvind Chauhan
 * @since: 20181102
 * @Usage: retrieve info from database and save it in a pre-defined directory
 */
@Controller

public class PlacementController {

    private final PlacementRepository pr;
    private final StudentRepository sr;
    private final BusinessRepository br;

    @Autowired
    public PlacementController(PlacementRepository pr, StudentRepository sr, BusinessRepository br) {
        this.pr = pr;
        this.sr = sr;
        this.br = br;
    }
    
    // Add Student
    @RequestMapping("/ojtOperations/placementAdd")
    public String placementAdd(Model model) {

        Placement newPlacement = new Placement();

        model.addAttribute("placement", newPlacement);

        model.addAttribute("businesses", br.findAll());
        //added to display student name during placement add
        model.addAttribute("students", sr.findAll());

        //This will send the user to the placementAdd.html page.
        return "ojtOperations/placementAdd";
    }

    //Issue28 - suggest just /placement/list for the request mapping.  
    
    @RequestMapping("/ojtOperations/placementList")
    public String placementList(Model model) {

        long size = pr.count();
        model.addAttribute("message", "Showing " + size + " of " + size + "");

        model.addAttribute("placements", pr.findAll());

        return "ojtOperations/placementList";
    }

    @RequestMapping("/ojtOperations/studentsNotPlacedList")
    public String studentNotPlacedList(Model model) {

        long size = pr.count();
        model.addAttribute("message", "Showing " + size + " of " + size + "");

        /* Issue28
        Do not think you can use the pr.findUnplacedStudent like this.  Suggest using the findall
        to find all students and then looping through each one.  For each one you want to 
        check the placement for the id of the student (studentId of the placement table).  Can 
        create and use a findByStudentId method in the PlacementRepository).  If the student is found in the 
        placement table then they are placed if not they should be shown on the report.  You can add 
        them to a separate ArrayList to be used on the form.
        */
        
        ArrayList<Student> sArrayList = (ArrayList<Student>) (sr.findAll());
        ArrayList<Placement> pArrayList = (ArrayList<Placement>) (pr.findAll());
        ArrayList<Student> newStudentList = (ArrayList<Student>) (sr.findAll());
        
        for (int i = 0; i < sArrayList.size(); i++) {
            for (int j = 0; j < pArrayList.size(); j++) {
                if (sArrayList.get(i).getId() == pArrayList.get(j).getStudentId()) {
                    newStudentList.remove(sArrayList.get(i));
                        }
                    }
                }
        
        model.addAttribute("students", newStudentList);
        return "ojtOperations/studentList";
    }
    //unplacedBusinessList

    @RequestMapping("/ojtOperations/unplacedBusinessList")
    public String unplacedBusinessList(Model model) {

        long size = pr.count();
        model.addAttribute("message", "Showing " + size + " of " + size + "");
        
        ArrayList<Business> bArrayList = (ArrayList<Business>) br.findAll();
        ArrayList<Placement> pArrayList = (ArrayList<Placement>) pr.findAll();
        ArrayList<Business> newBusinessList =  (ArrayList<Business>) br.findAll();
        
        for (int i = 0; i < bArrayList.size(); i++) {
            for (int j = 0; j < pArrayList.size(); j++) {
                if (bArrayList.get(i).getId() == pArrayList.get(j).getBusinessId()) {
                    newBusinessList.remove(bArrayList.get(i));
                }
            }
        }
        
        model.addAttribute("Business", newBusinessList);
        return "other/business/businessList";
    }

    // Update Placement
    @RequestMapping("/ojtOperations/placementUpdate")
    public String placementUpdate(Model model, HttpServletRequest request) {

        String idToFind = request.getParameter("id");
        Placement editPlacement = pr.findOne(Integer.parseInt(idToFind));
        model.addAttribute("placement", editPlacement);
        
        // This will send the user to the placementAdd.html page
        return "/ojtOperations/placementUpdate";

    }

    // Delete Placement
    @RequestMapping("/ojtOperations/placementDelete")
    public String placementDelete(Model model, HttpServletRequest request) {

        String idToFind = request.getParameter("id");

        pr.delete(Integer.parseInt(idToFind));
        model.addAttribute("placements", pr.findAll());

        // This will send the user to the studentList.html page
        return "/ojtOperations/placementList";

    }

    @RequestMapping("/ojtOperations/placementUpdateSubmit")
    public String ojtreflectionUpdateSubmit(Model model, @ModelAttribute("placement") Placement thePlacementFromTheForm, BindingResult result) {

        if (result.hasErrors()) {
            System.out.println("Error in validation.");
            return "/ojtOperations/placementAdd";
        }
        
        try {
            pr.save(thePlacementFromTheForm);
        } catch (Exception e) {
            System.out.println("Could not save to the database");
            e.printStackTrace(); //Issue28 put the stack trace out to the log.
        }
        model.addAttribute("placements", pr.findAll());

        //This will send the user to the placementList.html page.
        return "ojtOperations/placementList";
    }    
    
    @RequestMapping("/ojtOperations/placementAddSubmit")
    public String ojtreflectionAddSubmit(Model model, @ModelAttribute("placement") Placement thePlacementFromTheForm, BindingResult result) {

        if (result.hasErrors()) {
            System.out.println("Error in validation.");
            return "/ojtOperations/placementAdd";
        }
        
        try {
            //Issue28 need to see what is in the object before the save to see what's wrong.
            /*Issue28
            When you are adding a student, placement, or business - you may have issues 
            saving using the jpa repository if the id is null.  You can avoid this by 
            setting the id to 0 before doing the save. See below.
            */
            System.out.println(thePlacementFromTheForm);
            thePlacementFromTheForm.setId(0);
            pr.save(thePlacementFromTheForm);
        } catch (Exception e) {
            System.out.println("Could not save to the database");
            e.printStackTrace(); //Issue28 put the stack trace out to the log.
        }
        model.addAttribute("placements", pr.findAll());

        //This will send the user to the placementList.html page.
        return "ojtOperations/placementList";
    }
//     ------- Beginning of comment before JPA implementation ------
    //Get current date to be added to file
    Date dNow = new Date();
    SimpleDateFormat ft = new SimpleDateFormat("yyyyMMdd");
    //File location to be save output
    public String FILE_NAME = "C:/cis2232/ojt_" + ft.format(dNow) + ".txt";

    @RequestMapping("/placement/export")

    public String exportPlacement(Model model) {

        ArrayList<String> data = new ArrayList<String>();

        try {
            Connection con = null;
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/cis2232_ojt", "cis2232_admin", "Test1234");
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT B.name as BusinessName, "
                    + "CONCAT(S.firstName,' ',S.lastName) as StudentName "
                    + "FROM student S, business B, placement P "
                    + "WHERE s.id = P.studentId AND b.id = p.businessId");
            while (rs.next()) {
                String bName = rs.getString("BusinessName");
                String sName = rs.getString("StudentName");
                data.add(bName + " " + sName);
            }
            writeToFile(data, FILE_NAME);
            rs.close();
            st.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return "other/impExpConfirmationPlacement";
    }

    private static void writeToFile(java.util.List list, String path) throws IOException {
        BufferedWriter out = null;
        try {
            File file = new File(path);
            out = new BufferedWriter(new FileWriter(file, true));
            for (Object s : list) {
                out.write((String) s);
                out.newLine();
            }
            out.close();
        } catch (IOException e) {
            System.out.println(e);
        }
    }
    //----- end of comment before JPA implementation -------  
}
