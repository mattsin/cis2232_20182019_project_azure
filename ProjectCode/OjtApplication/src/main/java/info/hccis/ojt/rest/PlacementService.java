package info.hccis.ojt.rest;

import com.google.gson.Gson;
import info.hccis.ojt.data.springdatajpa.BusinessRepository;
import info.hccis.ojt.data.springdatajpa.StudentRepository;
import info.hccis.ojt.data.springdatajpa.PlacementRepository;
import info.hccis.ojt.web.BusinessController;
import info.hccis.ojt.web.PlacementController;
import info.hccis.ojt.web.StudentController;
import info.hccis.ojt.model.jpa.Business;
import info.hccis.ojt.model.jpa.Placement;
import info.hccis.ojt.model.jpa.Student;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Path("/PlacementService")
public class PlacementService {

    @Resource
    private final PlacementRepository pr;
    private final StudentRepository sr;
    private final BusinessRepository br;
    

    /**
     * Note that dependency injection (constructor) is used here to provide the
     * UserRepository object for use in this class.
     *
     * @param servletContext Context
     * @since 20180604
     * @author BJM
     */
    public PlacementService(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.pr = applicationContext.getBean(PlacementRepository.class);
        this.sr = applicationContext.getBean(StudentRepository.class);
        this.br = applicationContext.getBean(BusinessRepository.class);
    }

    /**
     * This rest service will provide all students who have been placed and their info from the DB.
     *
     * @return json string containing all student information.
     * @since 20181130
     * @author Arvind C
     */
    @GET
    @Path("/placement/showall")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlacementInfo() {
        
        
        ArrayList<Placement> placement = (ArrayList<Placement>) pr.findAll();
        
        Gson gson = new Gson();

        int statusCode = 200;
        if (placement.isEmpty()) {
            statusCode = 204;
        }

        String temp = "";
        temp = gson.toJson(placement);

        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }
    /**
     * This rest service will provide all businesses without an OJT student placed
     *
     * @return json string containing the user attributes.
     * @since 20181130
     * @author Arvind Chauhan
     */
    @GET
    @Path("/placement/unplacedBusiness")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUnplacedBusinessInfo() {
        
        ArrayList<Business> bArrayList = (ArrayList<Business>) br.findAll();
        ArrayList<Placement> pArrayList = (ArrayList<Placement>) pr.findAll();
        ArrayList<Business> newBusinessList =  (ArrayList<Business>) br.findAll();
        
        for (int i = 0; i < bArrayList.size(); i++) {
            for (int j = 0; j < pArrayList.size(); j++) {
                if (bArrayList.get(i).getId() == pArrayList.get(j).getBusinessId()) {
                    newBusinessList.remove(bArrayList.get(i));
                }
            }
        }
        
        Gson gson = new Gson();

        int statusCode = 200;
        if (newBusinessList.isEmpty()) {
            statusCode = 204;
        }

        String temp = "";
        temp = gson.toJson(newBusinessList);

        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }
    

}
