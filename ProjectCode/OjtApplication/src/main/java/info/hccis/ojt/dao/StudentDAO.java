/**
 * @author: Matt B
 * @since: 20181030
 * @Usage: StudentDAO to handle student importing
 */
package info.hccis.ojt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * @author: Matt Bryan 
 * @since: 20181030
 * @Usage: Update method to add students based on info pulled from
 * "student.csv" (See StudentController).
 */
public class StudentDAO {

    public static void update(String id, String lastName, String firstName) {

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        
        
        // Build SQL for inserting studends
        try {
            conn = ConnectionUtils.getDBConnection();

            sql = "INSERT INTO `Student`(`studentId`, `lastName`, `firstName`) " + "VALUES (?,?,?)";

            ps = conn.prepareStatement(sql);
            ps.setString(1, id);
            ps.setString(2, lastName);
            ps.setString(3, firstName);

            ps.executeUpdate();

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();

        } finally {
            DbUtils.close(ps, conn);
        }
    }

}
