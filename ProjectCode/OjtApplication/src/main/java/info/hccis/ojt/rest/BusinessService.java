package info.hccis.ojt.rest;

import com.google.gson.Gson;
import info.hccis.ojt.data.springdatajpa.BusinessRepository;
import info.hccis.ojt.data.springdatajpa.StudentRepository;
import info.hccis.ojt.model.jpa.Business;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Path("/BusinessService")
public class BusinessService {

    @Resource
    private final BusinessRepository br;

    /**
     * Note that dependency injection (constructor) is used here to provide the
     * UserRepository object for use in this class.
     *
     * @param servletContext Context
     * @since 20180604
     * @author BJM
     */
    public BusinessService(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.br = applicationContext.getBean(BusinessRepository.class);
    }

    /**
     * This rest service will provide all students and their info from the DB.
     *
     * @return json string containing all user information.
     * @since 20181130
     * @author Matt B
     */
    @GET
    @Path("/business/showall")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBusinessInfo() {

        ArrayList<Business> business = (ArrayList<Business>) br.findAll();
        
        Gson gson = new Gson();

        int statusCode = 200;
        if (business.isEmpty()) {
            statusCode = 204;
        }

        String temp = "";
        temp = gson.toJson(business);

        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }
    
    
     /**
     * This rest service will provide all students and their info from the DB.
     *
     * @param name
     * @param username
     * @return json string containing all user information.
     * @since 20181130
     * @author Matt B
     */
    @GET
    @Path("/business/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsername(@PathParam("name") String name) {
        
        ArrayList<Business> business = (ArrayList<Business>) br.findByName(name);

        Gson gson = new Gson();
        int statusCode = 200;
        
        if (business.isEmpty()) {
            statusCode = 204;
        }

        String temp = "";
        temp = gson.toJson(business);

        return Response.status(statusCode)
                .entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }

}
